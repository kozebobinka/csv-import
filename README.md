## CSV import

### For starting on local machine you need:

Add rule to file _hosts_ if you somewhy want to open it from browser:

    127.0.0.1 csv-import.localhost

For example, with command:

    $ echo "127.0.0.1 csv-import.localhost" >> /etc/hosts

Build container, enter there and install application:

    $ docker-compose build --no-cache 
    $ docker-compose up -d
    $ docker-compose exec php-fpm bash
    csv-import# composer install
    csv-import# php bin/console doctrine:migrations:migrate

Add _.env.local_ and _.env.test.local_ files with content:

    DATABASE_URL=mysql://root:root@mysql/importTest?serverVersion=8.0

Run tests:

    csv-import# ./bin/run-tests.sh

## Notes

### While solving a problem, an imaginary customer was asked the following questions (and my answers):

Q: Can there be other columns in the table? What to do with them?

A: Yes. Do not process, but do not consider an error

Q: Can the columns be in a different order?

A: Yes.

Q: Can there be numbers with a spaces inside (like 1 000.33)?

A: Yes.

Q: What should I do if a product with a specific code is already in the database?

A: Refresh item.

Q: What should I do if a product with a specific code is already in the database, but it is loading again with incorrect data?

A: Do not change in the database.

Q: What should I do if a product with specific data is already in the database, but it is loading with values that contradict the business logic condition (for example, it costs more than 1000)?

A: Remove item from database.

Q: What should I do if the same product code occurs 2 or more times in the file?

A: We record only the first one, mark the rest as duplicates and show them in the table of unloaded data.

Q: What characters should be processed?

A: The limitation of the database encoding does not allow loading any characters, therefore we will limit ourselves to the letters of the latin alphabet, numbers, punctuation marks and the sign ”

Q: Should the price be rounded to cents?

A: Yes.

### Could not find a normal solution for problems:

1. How to upload a CR line delimited file (/r).
2. How to filter characters available in latin-1 for MySQL with a regular expression. For some reason in PHP it's a different character set altogether, or I'm going crazy.