<?php

namespace App\Tests\Repository;

use App\DataFixtures\ProductDataFixtures;
use App\Entity\ProductData;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductDataRepositoryTest extends KernelTestCase
{
    private EntityManager $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testFindOrCreateByCode(): void
    {
        $productData = $this->entityManager
            ->getRepository(ProductData::class)
            ->findByCode(ProductDataFixtures::EXISTING_CODE);

        $this->assertNotNull($productData);

        $productData = $this->entityManager
            ->getRepository(ProductData::class)
            ->findByCode(ProductDataFixtures::NOT_EXISTING_CODE);

        $this->assertNull($productData);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
    }
}