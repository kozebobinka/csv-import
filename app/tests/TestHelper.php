<?php

namespace App\Tests;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;

class TestHelper
{
    public const VALID_FILE_CONTENT = <<<EOL
Product Code,Product Name,Product Description,Stock,Cost in GBP,Discontinued
P0001,TV,32” test Tv,11,399.99,
P0002,Cd Player,Nice CD player,11,50.12,yes
P0003,VCR,Top notch VCR,2,4.99,yes
P0004,Bluray Player,Watch it in HD,1,24.55,
P0005,XBOX360,Best.console.ever,5,30.44,
P0006,PS3,Mind your details,3,24.99,
P0007,24” Monitor,Awesome,,35.99,
EOL;

    /**
     * @param object $obj
     * @param string $name
     * @param array<int, mixed> $args
     * @return mixed
     * @throws \ReflectionException
     */
    public static function callMethod(object $obj, string $name, array $args): mixed
    {
        $class = new \ReflectionClass($obj);
        return $class->getMethod($name)->invokeArgs($obj, $args);
    }

    public static function createFile(string $fileContent): string
    {
        $filesystem = new Filesystem();
        $fileName = $filesystem->tempnam(Path::normalize(\sys_get_temp_dir()), 'stock_', '.csv');
        $filesystem->dumpFile($fileName, $fileContent);

        return $fileName;
    }
}