<?php

namespace App\Tests\Entity;

use App\DataFixtures\ProductDataFixtures;
use App\Dto\ProductDataDto;
use App\Entity\ProductData;
use App\Exception\ProductDataProcessException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductDataTest extends KernelTestCase
{
    private EntityManager $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);
    }

    /**
     * @throws ProductDataProcessException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function testCreateOrUpdateFromDto(): void
    {
        $productDataDto = (new ProductDataDto())
        ->setCode(ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD)
        ->setName('foo')
        ->setDescription('bar')
        ->setStock(1)
        ->setPrice(42)
        ->setDateDiscontinued('yes');

        $productData = ProductData::createOrUpdateFromDto($productDataDto);
        $this->entityManager->persist($productData);
        $this->entityManager->flush();

        $this->assertInstanceOf(\DateTimeInterface::class, $productData->getDateDiscontinued());
        $this->assertInstanceOf(\DateTimeInterface::class, $productData->getDateAdd());

        $existingProductData = $this->entityManager
            ->getRepository(ProductData::class)
            ->findByCode(ProductDataFixtures::EXISTING_CODE);

        $dateAdd = clone($existingProductData->getDateAdd());

        $productDataDto = (new ProductDataDto())
            ->setCode(ProductDataFixtures::EXISTING_CODE)
            ->setName(ProductDataFixtures::EXISTING_NAME)
            ->setDescription(ProductDataFixtures::EXISTING_DESCRIPTION)
            ->setStock(ProductDataFixtures::EXISTING_STOCK)
            ->setPrice(ProductDataFixtures::EXISTING_PRICE);

        $productData = ProductData::createOrUpdateFromDto($productDataDto, $existingProductData);
        $this->entityManager->persist($productData);
        $this->entityManager->flush();

        $this->assertEquals($dateAdd, $productData->getDateAdd());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if($this->entityManager->getConnection()->isTransactionActive()) {
            $this->entityManager->rollback();
        }

        $this->entityManager->close();
    }
}