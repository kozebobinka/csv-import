<?php

namespace App\Tests\Helpers;

use App\Helpers\DateTimeHelper;
use PHPUnit\Framework\TestCase;

class DateTimeHelperTest extends TestCase
{
    public function testCreateDate(): void
    {
        $date = DateTimeHelper::createDate();

        $this->assertInstanceOf(\DateTimeInterface::class, $date);
    }
}