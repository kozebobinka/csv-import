<?php

namespace App\Tests\Helpers;

use App\Helpers\CsvFileObject;
use App\Services\ProductDataService;
use App\Tests\TestHelper;
use PHPUnit\Framework\TestCase;

class CsvFileObjectTest extends TestCase
{
    public function testCsvFileObjectWrongFile(): void
    {
        $this->expectException(\RuntimeException::class);

        new CsvFileObject(TestHelper::createFile('test'), ProductDataService::HEADERS);
    }

    public function testCsvFileObjectRightFile(): void
    {
        $file = new CsvFileObject(TestHelper::createFile(TestHelper::VALID_FILE_CONTENT), ProductDataService::HEADERS);

        $this->assertIsArray($file->getHeaders());
        $this->assertIsArray($file->getHeadersTitles());
        $this->assertEquals(7, $file->getLinesCount());
    }
}