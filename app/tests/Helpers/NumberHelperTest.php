<?php

namespace App\Tests\Helpers;

use App\Helpers\NumberHelper;
use PHPUnit\Framework\TestCase;

class NumberHelperTest extends TestCase
{
    /**
     * @return array<int, array<int, mixed>>
     */
    private function testParseFloatDataProvider(): array
    {
        return [
            [' 11.2', 11.2],
            ['4', 4.0],
            ['1 144.2', 1144.2],
            ['1 144,2', 1144.2],
            ['1 144;2', null],
            [23.733, 23.73],
        ];
    }

    /**
     * @dataProvider testParseFloatDataProvider
     */
    public function testParseFloat(mixed $value, ?float $parsedValue): void
    {
        $this->assertEquals($parsedValue, NumberHelper::parsePrice($value));
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    private function testParseIntDataProvider(): array
    {
        return [
            ['11.2', null],
            ['4', 4],
            [4.3, null],
            ['1 144', 1144],
            ['test', null],
            [2, 2],
            [null, null],
        ];
    }

    /**
     * @dataProvider testParseIntDataProvider
     */
    public function testParseInt(mixed $value, ?int $parsedValue): void
    {
        $this->assertEquals($parsedValue, NumberHelper::parsePositiveInt($value));
    }
}