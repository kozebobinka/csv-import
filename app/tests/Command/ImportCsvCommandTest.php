<?php

namespace App\Tests\Command;

use App\Command\ImportCsvCommand;
use App\DataFixtures\ProductDataFixtures;
use App\Entity\ProductData;
use App\Helpers\CsvFileObject;
use App\Tests\Helpers\CsvFileObjectTest;
use App\Tests\TestHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Tester\CommandTester;

class ImportCsvCommandTest extends KernelTestCase
{
    private EntityManager $entityManager;
    private CommandTester $commandTester;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $application = new Application($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);

        $command = $application->find('app:import-csv');
        $this->commandTester = new CommandTester($command);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    private function testExecuteDataProvider(): array
    {
        return [
            [
                'Just text',
                Command::FAILURE,
                CsvFileObject::MESSAGE_ERROR_MISSING_COLUMNS,
            ],
            [ // Extra column, insert
                \sprintf(
                    'Product Code,Product Name,Product Description,Stock,Cost in GBP,Wrong column,Discontinued
                    %s,TV,32” Tv,10,399.99,test,yes',
                    ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 1, 0, 0, 0),
            ],
            [ // Columns order, insert
                \sprintf(
                    'Product Code,Product Description,Product Name,Cost in GBP,Stock,Discontinued
                    %s,32” Tv,TV,399.99,10,yes',
                    ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 1, 0, 0, 0),
            ],
            [ // Wrong row
                \sprintf(
                    'Product Code,Product Name,Product Description,Stock,Cost in GBP,Discontinued
                    %s,TV,32” Tv,10,399,99,yes',
                    ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 0, 0, 0, 1),
            ],
            [ // Semicolons, insert
                \sprintf(
                    'Product Code;Product Name;Product Description;Stock;Cost in GBP;Discontinued
                    %s;TV;32” Tv;10;399,99;yes;',
                    ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 1, 0, 0, 0),
            ],
            [ // Update existing
                \sprintf(
                    'Product Code,Product Name,Product Description,Stock,Cost in GBP,Discontinued
                    %s,TV,32” Tv,10,399.99,yes',
                    ProductDataFixtures::EXISTING_CODE
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 0, 1, 0, 0),
            ],
            [ // Check business logic 1, delete
                \sprintf(
                    'Product Code,Product Name,Product Description,Stock,Cost in GBP,Discontinued
                    %s,TV,32” Tv,10,%f,yes',
                    ProductDataFixtures::EXISTING_CODE,
                    ProductData::PRICE_MAX + 1.98
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 0, 0, 1, 1),
            ],
            [ // Check business logic 2
                \sprintf(
                    'Product Code,Product Name,Product Description,Stock,Cost in GBP,Discontinued
                    %s,TV,32” Tv,%d,%f,yes',
                    ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD,
                    ProductData::STOCK_FOR_PRICE_MIN - 1,
                    ProductData::PRICE_MIN - 0.08
                ),
                Command::SUCCESS,
                \sprintf(ImportCsvCommand::MESSAGE_FINISHED, 1, 0, 0, 0, 1),
            ],
        ];
    }

    /**
     * @dataProvider testExecuteDataProvider
     */
    public function testExecute(string $fileContent, int $statusCode, string $containsString): void
    {
        $this->commandTester->execute([
            'file' => TestHelper::createFile($fileContent),
        ]);

        $this->assertEquals($statusCode, $this->commandTester->getStatusCode());

        $output = $this->commandTester->getDisplay();
        $this->assertStringContainsString($containsString, $output);
    }

    public function testExecuteWithNoParams(): void
    {
        $this->expectException(RuntimeException::class);
        $this->commandTester->execute([]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if($this->entityManager->getConnection()->isTransactionActive()) {
            $this->entityManager->rollback();
        }

        $this->entityManager->close();
    }
}