<?php

namespace App\Tests\Dto;

use App\Dto\CommandOutput;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class CommandOutputTest extends TestCase
{
    public function testAddRowMethod(): void
    {
        $io = $this->createMock(OutputInterface::class);
        $commandOutput = new CommandOutput(new ProgressBar($io, 42));

        $row = ['code' => 'P0004', 'name' => ''];
        $fields = ['name'];
        $commandOutput->addErrorRow(42, $row, $fields);

        $this->assertEquals('<error>!</error>', $commandOutput->errorRows[42]['name']);


        $row = ['code' => 'P004', 'name' => 'foo'];
        $fields = ['code'];
        $commandOutput->addErrorRow(42, $row, $fields, 'DUPLICATE!');

        $this->assertEquals('<error>P004 DUPLICATE!</error>', $commandOutput->errorRows[42]['code']);
    }
}