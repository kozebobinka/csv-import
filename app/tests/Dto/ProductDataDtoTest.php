<?php

namespace App\Tests\Dto;

use App\Dto\ProductDataDto;
use App\Entity\ProductData;
use App\Exception\ProductDataProcessException;
use App\Helpers\DateTimeHelper;
use App\Helpers\NumberHelper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Validation;

class ProductDataDtoTest extends TestCase
{
    /**
     * @return array<int, array<int, mixed>>
     */
    private function testValidationDataProvider(): array
    {
        return [
            [
                [
                    ProductData::PATH_CODE => 'P0010',
                    ProductData::PATH_NAME => 'CD Bundle""',
                    ProductData::PATH_DESCRIPTION => 'Lots of fun',
                    ProductData::PATH_STOCK => '1',
                    ProductData::PATH_PRICE => 10,
                    ProductData::PATH_DATE_DISCONTINUED => null,
                ],
                0,
                0,
                0,
            ],
            [
                [
                    ProductData::PATH_CODE => '',
                    ProductData::PATH_NAME => 'CD Bundle ы',
                    ProductData::PATH_DESCRIPTION => 'Lots of fun',
                    ProductData::PATH_STOCK => 2,
                    ProductData::PATH_PRICE => 1000.11,
                    ProductData::PATH_DATE_DISCONTINUED => '',
                ],
                0,
                2,
                1,
            ],
            [
                [
                    ProductData::PATH_CODE => '',
                    ProductData::PATH_NAME => 'CD Bundle ы',
                    ProductData::PATH_DESCRIPTION => 'Lots of fun',
                    ProductData::PATH_STOCK => 2,
                    ProductData::PATH_PRICE => 1000.11,
                    ProductData::PATH_DATE_DISCONTINUED => 'null',
                ],
                1,
                2,
                1,
            ],
            [
                [
                    ProductData::PATH_CODE => 'P0011',
                    ProductData::PATH_NAME => 'Ιερογλυφικά',
                    ProductData::PATH_DESCRIPTION => 'Lots of fun',
                    ProductData::PATH_STOCK => 2,
                    ProductData::PATH_PRICE => 4.99,
                    ProductData::PATH_DATE_DISCONTINUED => DateTimeHelper::createDate(),
                ],
                0,
                1,
                2,
            ],
        ];
    }

    /**
     * @dataProvider testValidationDataProvider
     *
     * @param array<string, mixed> $data
     * @param int $errorsExceptionCount
     * @param int $errorsDataCount
     * @param int $errorsBusinessLogic
     * @return void
     */
    public function testValidation(array $data, int $errorsExceptionCount, int $errorsDataCount, int $errorsBusinessLogic): void
    {
        $serializer = new Serializer([new ObjectNormalizer()]);

        try {
            /** @var ProductDataDto $productDataDto */
            $productDataDto = $serializer->denormalize($data, ProductDataDto::class);
        } catch (ExceptionInterface $e) {
            $this->assertInstanceOf(ProductDataProcessException::class, $e);
            /** @var ProductDataProcessException $e */
            $this->assertCount($errorsExceptionCount, $e->getFields(), 'Exception fields');

            return;
        }

        $this->assertEquals($productDataDto->getPrice(), NumberHelper::parsePrice($data[ProductData::PATH_PRICE]), 'Validate price');

        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();

        $errors = $validator->validate($productDataDto, null, [ProductDataDto::VALIDATION_GROUP_DATA]);
        $this->assertCount($errorsDataCount, $errors, 'Validate data');

        $errors = $validator->validate($productDataDto, null, [ProductDataDto::VALIDATION_GROUP_BUSINESS_LOGIC]);
        $this->assertCount($errorsBusinessLogic, $errors, 'Validate business logic');
    }
}