<?php

namespace App\Tests\Services;

use App\DataFixtures\ProductDataFixtures;
use App\Dto\CommandOutput;
use App\Entity\ProductData;
use App\Exception\ProductDataDuplicateException;
use App\Exception\ProductDataProcessException;
use App\Helpers\CsvFileObject;
use App\Services\ProductDataService;
use App\Tests\TestHelper;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

class ProductDataServiceTest extends KernelTestCase
{
    /**
     * @var array<int, string>
     */
    private array $indexedHeaders = [
        0 => ProductData::PATH_CODE,
        1 => ProductData::PATH_NAME,
        2 => ProductData::PATH_DESCRIPTION,
        4 => ProductData::PATH_STOCK,
        5 => ProductData::PATH_PRICE,
        6 => ProductData::PATH_DATE_DISCONTINUED,
    ];

    private ProductDataService $productDataService;
    private EntityManager $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->entityManager->beginTransaction();
        $this->entityManager->getConnection()->setAutoCommit(false);

        $this->productDataService = $kernel->getContainer()->get(ProductDataService::class);
    }

    /**
     * @return array<int, array<int, mixed>>
     */
    private function testProcessRowMethodProvider(): array
    {
        return [
            [
                [
                    0 => ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD,
                    1 => 'foo',
                    2 => 'bar',
                    4 => '12',
                    5 => '12. 7',
                ],
                false,
            ],
            [
                [
                    0 => ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD,
                    1 => 'foo',
                    2 => 'bar',
                    4 => '12',
                    5 => '100.1',
                    6 => 'Yes',
                ],
                false,
            ],
            [
                [
                    0 => ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD,
                    1 => 'foo',
                    2 => 'bar',
                    4 => '12',
                    5 => '100.1',
                    6 => 'test',
                ],
                true,
            ],
            [
                [
                    0 => ProductDataFixtures::NOT_EXISTING_CODE_TO_ADD,
                    1 => 'foo',
                    2 => 'bar',
                    4 => '12',
                    5 => 'rer',
                ],
                true,
            ],
            [
                [
                    0 => '',
                    1 => 'foo',
                    2 => 'bar',
                    4 => '12',
                    5 => 'rer',
                ],
                true,
            ],
            [
                [
                    0 => ProductDataFixtures::EXISTING_CODE,
                    1 => 'foo',
                    2 => 'bar',
                    4 => '12',
                    5 => ProductData::PRICE_MAX + 0.01,
                ],
                true,
            ],
            [
                [
                    0 => ProductDataFixtures::EXISTING_CODE,
                    1 => 'ыйж',
                    2 => 'bar',
                    4 => '12',
                    5 => ProductData::PRICE_MAX + 0.01,
                ],
                true,
            ],
        ];
    }

    /**
     * @dataProvider testProcessRowMethodProvider
     *
     * @param array<int, mixed> $row
     * @param bool $isException
     * @return void
     *
     * @throws ProductDataDuplicateException
     * @throws ProductDataProcessException
     * @throws \ReflectionException
     */
    public function testProcessRowMethod(array $row, bool $isException): void
    {
        if ($isException) {
            $this->expectException(ProductDataProcessException::class);
        }

        $dateDiscontinued = $row[6] ?? '';

        $row = TestHelper::callMethod(
            $this->productDataService,
            'indexRow',
            [$row, $this->indexedHeaders]
        );
        $productData = TestHelper::callMethod(
            $this->productDataService,
            'processRow',
            [$row]
        );

        $this->assertEquals($productData->getCode(), $row[ProductData::PATH_CODE]);

        if (\strtolower($dateDiscontinued) === 'yes') {
            $this->assertInstanceOf(\DateTimeInterface::class, $productData->getDateDiscontinued());
        } else {
            $this->assertNull($productData->getDateDiscontinued());
        }
    }

    public function testProcessFileMethod(): void
    {
        $file = new CsvFileObject(TestHelper::createFile(
            TestHelper::VALID_FILE_CONTENT),
            ProductDataService::HEADERS
        );

        $io = $this->createMock(OutputInterface::class);
        $commandOutput = new CommandOutput(new ProgressBar($io, $file->getLinesCount() ?? 0));
        $this->productDataService->processFile($file, $commandOutput);

        $this->assertEquals(
            5,
            $this->entityManager->getRepository(ProductData::class)->count([]),
            'Count after first run.'
        );

        $this->assertEquals(7, $commandOutput->linesProcessed, 'Processed rows.');
        $this->assertEquals(4, $commandOutput->linesInserted, 'Inserted rows.');
        $this->assertEquals(1, $commandOutput->linesUpdated, 'Updated rows.');
        $this->assertEquals(1, $commandOutput->linesDeleted, 'Deleted rows.');

        $this->productDataService->processFile($file);

        $this->assertEquals(
            5,
            $this->entityManager->getRepository(ProductData::class)->count([]),
            'Count after second run.'
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        if($this->entityManager->getConnection()->isTransactionActive()) {
            $this->entityManager->rollback();
        }

        $this->entityManager->close();
    }
}