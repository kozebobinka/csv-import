#!/usr/bin/env bash

set -ex \
  && composer install \
  && php bin/console --env=test cache:clear \
  && php bin/console -q --env=test doctrine:database:create \
  && php bin/console -q --env=test doctrine:schema:create \
  && php bin/console -q --env=test doctrine:fixtures:load \
  && php vendor/bin/phpstan analyse \
  && php bin/phpunit \
  && php bin/console -q --env=test doctrine:database:drop --force

