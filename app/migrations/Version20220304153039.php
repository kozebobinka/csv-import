<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220304153039 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add stock and price';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tblProductData ADD intStock INT DEFAULT NULL AFTER strProductCode, ADD fltPrice DOUBLE PRECISION DEFAULT NULL AFTER intStock');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tblProductData DROP intStock, DROP fltPrice');
    }
}
