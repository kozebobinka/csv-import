<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220304184505 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Price is mandatory';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE tblProductData SET fltPrice = 0 WHERE fltPrice IS NULL');
        $this->addSql('ALTER TABLE tblProductData CHANGE fltPrice fltPrice DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tblProductData CHANGE fltPrice fltPrice DOUBLE PRECISION DEFAULT NULL');
    }
}
