<?php

namespace App\Helpers;

class DateTimeHelper
{
    /* @noinspection PhpUnhandledExceptionInspection */
    public static function createDate(): \DateTime
    {
        return new \DateTime('now', new \DateTimeZone('UTC'));
    }
}