<?php

namespace App\Helpers;

final class CsvFileObject extends \SplFileObject
{
    public const MESSAGE_ERROR_MISSING_COLUMNS = 'Missing columns';

    private const SUPPORTED_MIME_TYPES = [
        'text/csv',
        'text/plain',
    ];
    private const SUPPORTED_SEPARATORS = [
        ',',
        ';',
    ];

    /** @var array<int, string> */
    private array $headers = [];
    /** @var array<int, string> */
    private array $headersTitles = [];
    /** @var array<int, string> */
    private array $missedHeaders = [];
    private ?int $linesCount = null;

    /**
     * @param string $filename
     * @param array<string, string> $expectedHeaders
     */
    public function __construct(string $filename, array $expectedHeaders)
    {
        parent::__construct($filename);

        try {
            $mimeType = \mime_content_type($this->getPathname());
        } catch (\Exception) {
            throw new \RuntimeException('Can\'t read mime type');
        }

        if (!\in_array($mimeType, self::SUPPORTED_MIME_TYPES, true)) {
            throw new \RuntimeException('File type id not supported');
        }

        if (\count($expectedHeaders) === 0) {
            throw new \RuntimeException('Expected headers are empty');
        }

        $this->openFile();
        $this->setFlags(self::SKIP_EMPTY | self::DROP_NEW_LINE);

        $this->detectAndSetCsvControlByHeaders($expectedHeaders);
        $this->getLinesCount();
        $this->normalizeHeaders($expectedHeaders);

        if (\count($this->missedHeaders) > 0) {
            throw new \RuntimeException(\sprintf(
                '%s: %s',
                self::MESSAGE_ERROR_MISSING_COLUMNS,
                \implode(', ', $this->missedHeaders)
            ));
        }
    }

    /**
     * @return array<int, string>
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return array<int, string>
     */
    public function getHeadersTitles(): array
    {
        return $this->headersTitles;
    }

    public function getLinesCount(): ?int
    {
        if ($this->linesCount !== null) {
            return $this->linesCount;
        }

        $this->seek(PHP_INT_MAX);
        $this->linesCount = $this->key();
        $this->rewind();

        return $this->linesCount;
    }

    /**
     * @param array<string, string> $expectedHeaders
     * @return void
     */
    private function detectAndSetCsvControlByHeaders(array $expectedHeaders): void
    {
        $countOfExpectedHeaders = \count($expectedHeaders);

        foreach (self::SUPPORTED_SEPARATORS as $separator) {
            $this->headers = $this->fgetcsv($separator) ?: [];
            $this->rewind();
            if (\count($this->headers) >= $countOfExpectedHeaders) {
                $this->setCsvControl($separator);

                return;
            }
        }
    }

    /**
     * @param array<string, string> $expectedHeaders
     * @return void
     */
    private function normalizeHeaders(array $expectedHeaders): void
    {
        $bom = \pack('CCC', 0xEF, 0xBB, 0xBF);

        foreach ($this->headers as $index => $header) {
            if ($index === 0 && \str_starts_with($header, $bom)) {
                $header = \substr($header, 3);
            }
            $normalizedHeader = \ucwords(\strtolower((string)\preg_replace('/\s+/', ' ', $header)));
            if (isset($expectedHeaders[$normalizedHeader])) {
                $this->headers[$index] = $expectedHeaders[$normalizedHeader];
                $this->headersTitles[] = $header;
            }
        }

        $indexedHeaders = \array_intersect($this->headers, $expectedHeaders);
        $this->missedHeaders = \array_keys(\array_diff($expectedHeaders, $indexedHeaders));

        if (\count($this->missedHeaders) > 0) {
            $this->headers = [];
            return;
        }

        $this->headers = $indexedHeaders;
    }
}