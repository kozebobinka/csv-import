<?php

namespace App\Helpers;

class NumberHelper
{
    public static function parsePrice(mixed $value): ?float
    {
        $value = \str_replace([',', ' '], ['.', ''], $value);
        return (\is_numeric($value) ? \round((float)$value, 2) : null);
    }

    public static function parsePositiveInt(mixed $value): ?int
    {
        $value = (string)$value;
        $value = \str_replace([' '], [''], $value);
        return (\preg_match('/^\d+$/', $value) ? (int)$value : null);
    }
}