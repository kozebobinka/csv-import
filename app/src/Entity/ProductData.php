<?php

namespace App\Entity;

use App\Dto\ProductDataDto;
use App\Helpers\DateTimeHelper;
use App\Repository\ProductDataRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: "tblProductData")]
#[ORM\UniqueConstraint(name: "strProductCode", columns: ["strProductCode"])]
#[ORM\Entity(repositoryClass: ProductDataRepository::class)]
#[ORM\HasLifecycleCallbacks]
class ProductData
{
    public const PATH_CODE = 'code';
    public const PATH_NAME = 'name';
    public const PATH_DESCRIPTION = 'description';
    public const PATH_STOCK = 'stock';
    public const PATH_PRICE = 'price';
    public const PATH_DATE_DISCONTINUED = 'dateDiscontinued';

    public const PRICE_MAX = 1000;
    public const PRICE_MIN = 5;
    public const STOCK_FOR_PRICE_MIN = 10;

    #[ORM\Column(name: "intProductDataId", type: Types::INTEGER, nullable: false, options: ["unsigned" => true])]
    #[ORM\Id, ORM\GeneratedValue(strategy: 'IDENTITY')]
    private ?int $id = null;

    #[ORM\Column(name: "strProductName", type: Types::STRING, length: 50, nullable: false)]
    private ?string $name = null;

    #[ORM\Column(name: "strProductDesc", type: Types::STRING, length: 255, nullable: false)]
    private ?string $description = null;

    #[ORM\Column(name: "strProductCode", type: Types::STRING, length: 10, nullable: false)]
    private ?string $code = null;

    #[ORM\Column(name: "intStock", type: Types::INTEGER, nullable: true)]
    private ?int $stock = null;

    #[ORM\Column(name: "fltPrice", type: Types::FLOAT, nullable: false)]
    private ?float $price = null;

    #[ORM\Column(name: "dtmAdded", type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateAdd = null;

    #[ORM\Column(name: "dtmDiscontinued", type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $dateDiscontinued = null;

    #[ORM\Column(name: "stmTimestamp", type: Types::DATETIME_MUTABLE, nullable: false, options: ["default" => "CURRENT_TIMESTAMP"])]
    private ?\DateTimeInterface $timestamp;

    private function __construct() {}

    public static function createOrUpdateFromDto(
        ProductDataDto $productDataDto,
        ?ProductData $productData = null
    ): ProductData
    {
        if ($productData === null) {
            $productData = new self();
            $productData->timestamp = DateTimeHelper::createDate();
        }

        $productData->code = $productDataDto->getCode();
        $productData->name = $productDataDto->getName();
        $productData->description = $productDataDto->getDescription();
        $productData->stock = $productDataDto->getStock();
        $productData->price = $productDataDto->getPrice();
        $productData->dateDiscontinued = $productData->dateDiscontinued ?: $productDataDto->getDateDiscontinued();

        return $productData;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->dateAdd;
    }

    #[ORM\PrePersist]
    public function setDateAdd(): self
    {
        $this->dateAdd = new \DateTimeImmutable();

        return $this;
    }

    public function getDateDiscontinued(): ?\DateTimeInterface
    {
        return $this->dateDiscontinued;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }
}
