<?php

namespace App\DataFixtures;

use App\Dto\ProductDataDto;
use App\Entity\ProductData;
use App\Exception\ProductDataProcessException;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductDataFixtures extends Fixture
{
    public const EXISTING_CODE = 'P0001';
    public const EXISTING_NAME = 'foo';
    public const EXISTING_DESCRIPTION = 'bar';
    public const EXISTING_STOCK = 1;
    public const EXISTING_PRICE = 100.56;

    public const NOT_EXISTING_CODE_TO_ADD = 'P0002';
    public const NOT_EXISTING_CODE_TO_DELETE = 'P0003';
    public const NOT_EXISTING_CODE = 'X000X';

    /**
     * @throws ProductDataProcessException
     */
    public function load(ObjectManager $manager): void
    {
        $productDataDto = (new ProductDataDto())
            ->setCode(self::EXISTING_CODE)
            ->setName(self::EXISTING_NAME)
            ->setDescription(self::EXISTING_DESCRIPTION)
            ->setStock(self::EXISTING_STOCK)
            ->setPrice(self::EXISTING_PRICE);

        $productData = ProductData::createOrUpdateFromDto($productDataDto);

        $manager->persist($productData);

        $productDataDto = (new ProductDataDto())
            ->setCode(self::NOT_EXISTING_CODE_TO_DELETE)
            ->setName('foo')
            ->setDescription('bar')
            ->setStock(20)
            ->setPrice(4);

        $productData = ProductData::createOrUpdateFromDto($productDataDto);

        $manager->persist($productData);

        $manager->flush();
    }
}
