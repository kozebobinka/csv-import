<?php

namespace App\Exception;

use JetBrains\PhpStorm\Pure;

class ProductDataDuplicateException extends \Exception
{
    public const MESSAGE_CODE_DUPLICATE_CODE = 'Duplicate!';

    #[Pure] public function __construct(
        string $message = self::MESSAGE_CODE_DUPLICATE_CODE,
        int $code = 0,
        ?\Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}