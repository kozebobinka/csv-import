<?php

namespace App\Exception;

use App\Entity\ProductData;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class ProductDataProcessException extends \Exception implements ExceptionInterface
{
    public const CODE_INVALID_VALUE = 0;
    public const CODE_VALIDATION_DATA = 1;
    public const CODE_VALIDATION_BUSINESS_LOGIC = 2;
    public const CODE_UNKNOWN_ERROR = 4;

    public const MESSAGE_INVALID_VALUE = 'Invalid value.';
    public const MESSAGE_VALIDATION_DATA = 'Validation error.';
    public const MESSAGE_UNKNOWN_ERROR = 'Unknown error.';

    /** @var array<int, string> */
    private array $fields;

    /**
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     * @param array<int, string> $fields
     */
    #[Pure] public function __construct(string $message = "", int $code = 0, ?\Throwable $previous = null, array $fields = [])
    {
        $this->fields = $fields;
        parent::__construct($message, $code, $previous);
    }

    #[Pure] public static function forInvalidValue(string $field): self
    {
        return new self(
            self::MESSAGE_INVALID_VALUE,
            self::CODE_INVALID_VALUE,
            null,
            [$field]
        );
    }

    /**
     * @param array<int, string> $fields
     * @return ProductDataProcessException
     */
    #[Pure] public static function forDataValidation(array $fields): self
    {
        return new self(
            self::MESSAGE_VALIDATION_DATA,
            self::CODE_VALIDATION_DATA,
            null,
            $fields
        );
    }

    /**
     * @param array<int, string> $fields
     * @return ProductDataProcessException
     */
    #[Pure] public static function forBusinessLogicValidation(array $fields): self
    {
        return new self(
            self::MESSAGE_VALIDATION_DATA,
            self::CODE_VALIDATION_BUSINESS_LOGIC,
            null,
            $fields
        );
    }

    #[Pure] public static function forUnknownError(?\Throwable $previous): self
    {
        return new self(
            self::MESSAGE_UNKNOWN_ERROR,
            self::CODE_UNKNOWN_ERROR,
            $previous,
        );
    }

    /**
     * @return array<int, string>
     */
    public function getFields(): array
    {
        return $this->fields;
    }

}