<?php

namespace App\Command;

use App\Dto\CommandOutput;
use App\Helpers\CsvFileObject;
use App\Services\ProductDataService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCsvCommand extends Command
{
    protected static $defaultName = 'app:import-csv';

    public const MESSAGE_FINISHED = 'Finished! %d rows were processed, %d inserted, %d updated, %d deleted, %d skipped.';

    private SymfonyStyle $io;
    private CsvFileObject $file;

    public function __construct(private ProductDataService $productDataService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('This command allows you to upload CSV file.')
            ->addOption('dry-run', 'd', InputOption::VALUE_NONE, 'Do not save any data to database.')
            ->addArgument('file', InputArgument::REQUIRED, 'Path to the file');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->newline();

        $dryRun = $input->getOption('dry-run');
        if ($dryRun) {
            $this->io->info('Dry run mode. No changes in database would be done.');
        }

        try {
            $this->file = new CsvFileObject($input->getArgument('file'), ProductDataService::HEADERS);
        } catch (\Exception $e) {
            $this->io->error($e->getMessage());
            return Command::FAILURE;
        }

        $commandOutput = new CommandOutput(new ProgressBar($this->io, $this->file->getLinesCount() ?? 0));

        $this->productDataService->processFile($this->file, $commandOutput, $dryRun);

        $this->printReport($commandOutput);

        return Command::SUCCESS;
    }

    private function printReport(CommandOutput $commandOutput): void
    {
        $this->io->newline();

        $countErrors = \count($commandOutput->errorRows);

        $this->io->success(\sprintf(
            self::MESSAGE_FINISHED,
            $commandOutput->linesProcessed,
            $commandOutput->linesInserted,
            $commandOutput->linesUpdated,
            $commandOutput->linesDeleted,
            $countErrors
        ));

        if ($countErrors > 0) {
            $this->io->writeln('Skipped rows:');
            $this->io->table($this->file->getHeadersTitles(), $commandOutput->errorRows);
        }
    }
}