<?php

namespace App\Dto;

use App\Entity\ProductData;
use App\Exception\ProductDataProcessException;
use App\Helpers\DateTimeHelper;
use App\Helpers\NumberHelper;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProductDataDto
{

    public const VALIDATION_GROUP_DATA = 'data';
    public const VALIDATION_GROUP_BUSINESS_LOGIC = 'businessLogic';

    private const MESSAGE_FOR_PRICE_MAX = 'Any stock items which cost over $1000 will not be imported.';
    private const MESSAGE_FOR_PRICE_MIN = 'Any stock item which costs less that $5 and has less than 10 stock will not be imported.';

    private const DISCONTINUED_FLAG = 'yes';

    #[Assert\NotBlank(groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Length(max: 10, groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Regex('/^[\d\p{Latin}]+$/', groups: [self::VALIDATION_GROUP_DATA])]
    private ?string $code = null;

    #[Assert\NotBlank(groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Length(max: 50, groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Regex('/^[\s\d\p{Latin}\p{P}”]+$/', groups: [self::VALIDATION_GROUP_DATA])]
    private ?string $name = null;

    #[Assert\NotBlank(groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Length(max: 255, groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Regex('/^[\s\d\p{Latin}\p{P}”]+$/', groups: [self::VALIDATION_GROUP_DATA])]
    private ?string $description = null;

    #[Assert\PositiveOrZero(groups: [self::VALIDATION_GROUP_DATA])]
    private ?int $stock = null;

    #[Assert\NotNull(groups: [self::VALIDATION_GROUP_DATA])]
    #[Assert\Positive(groups: [self::VALIDATION_GROUP_DATA])]
    private ?float $price = null;

    private ?\DateTimeInterface $dateDiscontinued = null;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(mixed $code): ProductDataDto
    {
        $this->code = \trim((string)$code);

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(mixed $name): ProductDataDto
    {
        $this->name = \trim((string)$name);
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(mixed $description): ProductDataDto
    {
        $this->description = \trim((string)$description);

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    /**
     * @throws ProductDataProcessException
     */
    public function setStock(mixed $stock): ProductDataDto
    {
        $this->stock = NumberHelper::parsePositiveInt($stock);

        if ($this->stock === null) {
            throw ProductDataProcessException::forInvalidValue(ProductData::PATH_STOCK);
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @throws ProductDataProcessException
     */
    public function setPrice(mixed $price): ProductDataDto
    {
        $this->price = NumberHelper::parsePrice($price);

        if ($this->price === null) {
            throw ProductDataProcessException::forInvalidValue(ProductData::PATH_PRICE);
        }

        return $this;
    }

    public function getDateDiscontinued(): ?\DateTimeInterface
    {
        return $this->dateDiscontinued;
    }

    /**
     * @throws ProductDataProcessException
     */
    public function setDateDiscontinued(mixed $dateDiscontinued): ProductDataDto
    {
        $this->dateDiscontinued = match (true) {
            $dateDiscontinued instanceof \DateTimeInterface => $dateDiscontinued,
            \strtolower(\trim((string)$dateDiscontinued)) === self::DISCONTINUED_FLAG => DateTimeHelper::createDate(),
            (string)$dateDiscontinued === '' => null,
            default => throw ProductDataProcessException::forInvalidValue(ProductData::PATH_DATE_DISCONTINUED),
        };

        return $this;
    }

    #[Assert\Callback(groups: [self::VALIDATION_GROUP_BUSINESS_LOGIC])]
    public function priceValidation(ExecutionContextInterface $context): void
    {
        if ($this->price > ProductData::PRICE_MAX) {
            $context->buildViolation(self::MESSAGE_FOR_PRICE_MAX)
                ->atPath(ProductData::PATH_PRICE)
                ->addViolation();
        }
        if ($this->price < ProductData::PRICE_MIN && $this->stock < ProductData::STOCK_FOR_PRICE_MIN) {
            $context->buildViolation(self::MESSAGE_FOR_PRICE_MIN)
                ->atPath(ProductData::PATH_PRICE)
                ->addViolation();
            $context->buildViolation(self::MESSAGE_FOR_PRICE_MIN)
                ->atPath(ProductData::PATH_STOCK)
                ->addViolation();
        }
    }
}