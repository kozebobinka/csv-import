<?php

namespace App\Dto;

use Symfony\Component\Console\Helper\ProgressBar;

class CommandOutput
{
    /** @var array<int, array<string, string>> */
    public array $errorRows = [];
    public int $linesProcessed = 0;
    public int $linesInserted = 0;
    public int $linesUpdated = 0;
    public int $linesDeleted = 0;

    public function __construct(public ProgressBar $progressBar) {}

    /**
     * @param int $index
     * @param array<string, string> $row
     * @param array<int, string> $fields
     * @param string $errorSuffix
     */
    public function addErrorRow(int $index, array $row, array $fields, string $errorSuffix = ''): void
    {
        foreach ($fields as $field) {
            $row[$field] = (!isset($row[$field]) || $row[$field] === '')
                ? '<error>!</error>'
                : \sprintf('<error>%s %s</error>', $row[$field], $errorSuffix);
        }

        $this->errorRows[$index] = $row;
    }

}