<?php

namespace App\Services;

use App\Dto\CommandOutput;
use App\Dto\ProductDataDto;
use App\Entity\ProductData;
use App\Exception\ProductDataDuplicateException;
use App\Exception\ProductDataProcessException;
use App\Helpers\CsvFileObject;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductDataService
{
    public const HEADERS = [
        'Product Code' => ProductData::PATH_CODE,
        'Product Name' => ProductData::PATH_NAME,
        'Product Description' => ProductData::PATH_DESCRIPTION,
        'Stock' => ProductData::PATH_STOCK,
        'Cost In Gbp' => ProductData::PATH_PRICE,
        'Discontinued' => ProductData::PATH_DATE_DISCONTINUED,
    ];

    /** @var array<int, string> */
    private array $usedCodes = [];

    private Serializer $serializer;

    public function __construct(
        private ValidatorInterface     $validator,
        private EntityManagerInterface $entityManager
    )
    {
        $this->serializer = new Serializer([new ObjectNormalizer()]);
    }

    public function processFile(CsvFileObject $file, ?CommandOutput $commandOutput = null, bool $dryRun = false): void
    {
        $file->seek(1);

        while (!$file->eof()) {
            try {
                if (!$row = $file->fgetcsv()) {
                    continue;
                }
                if ($commandOutput !== null) {
                    $commandOutput->linesProcessed++;
                }
                $row = $this->indexRow($row, $file->getHeaders());
                $this->checkCode($row[ProductData::PATH_CODE] ?? null);
                $productData = $this->processRow($row);
                $this->entityManager->persist($productData);
            } catch (ProductDataProcessException $e) {
                $commandOutput?->addErrorRow($file->key() + 1, $row, $e->getFields());
            } catch (ProductDataDuplicateException $e) {
                $commandOutput?->addErrorRow($file->key() + 1, $row, [ProductData::PATH_CODE], $e->getMessage());
            }

            $commandOutput?->progressBar->advance();
        }

        if ($commandOutput !== null) {
            $this->countStatistics($commandOutput);
        }

        if (!$dryRun) {
            $this->entityManager->flush();
        }

        $commandOutput?->progressBar->finish();
    }

    /**
     * @param array<string, string> $row
     * @return ProductData
     *
     * @throws ProductDataDuplicateException
     * @throws ProductDataProcessException
     */
    private function processRow(array $row): ProductData
    {
        try {
            /** @var ProductDataDto $productDataDto */
            $productDataDto = $this->serializer->denormalize($row, ProductDataDto::class);
        } catch (ExceptionInterface $e) {
            if ($e instanceof ProductDataProcessException) {
                throw $e;
            }
            throw ProductDataProcessException::forUnknownError($e);
        }

        $productData = $this->entityManager->getRepository(ProductData::class)->findByCode((string)$productDataDto->getCode());

        $errors = $this->validator->validate($productDataDto, null, [ProductDataDto::VALIDATION_GROUP_DATA]);
        if (\count($errors) > 0) {
            throw ProductDataProcessException::forDataValidation($this->getErrorsFields($errors));
        }

        $errors = $this->validator->validate($productDataDto, null, [ProductDataDto::VALIDATION_GROUP_BUSINESS_LOGIC]);
        if (\count($errors) > 0) {
            $this->removeProductData($productData);
            throw ProductDataProcessException::forBusinessLogicValidation($this->getErrorsFields($errors));
        }

        return ProductData::createOrUpdateFromDto($productDataDto, $productData);
    }

    /**
     * Get array of names of fields with wrong values
     *
     * @param ConstraintViolationListInterface $errors
     * @return array<int, string>
     */
    private function getErrorsFields(ConstraintViolationListInterface $errors): array
    {
        return \array_map(
            static fn(ConstraintViolationInterface $error) => $error->getPropertyPath(),
            \iterator_to_array($errors)
        );
    }

    /**
     * @param array<int, string> $row
     * @param array<int, string> $indexedHeaders
     * @return array<string, string>
     */
    private function indexRow(array $row, array $indexedHeaders): array
    {
        $result = [];
        foreach ($row as $index => $value) {
            if (isset($indexedHeaders[$index])) {
                $result[$indexedHeaders[$index]] = $value;
            }
        }

        return $result;
    }

    /**
     * Checking duplicates of code in given file
     *
     * @throws ProductDataDuplicateException
     */
    private function checkCode(?string $code): void
    {
        if ($code === null || $code === '') {
            return;
        }

        if (\in_array($code, $this->usedCodes, true)) {
            throw new ProductDataDuplicateException();
        }

        $this->usedCodes[] = $code;
    }

    /**
     * If existing entity comes with new data which is wrong for business logic
     */
    private function removeProductData(?ProductData $productData): void
    {
        if ($productData !== null) {
            $this->entityManager->remove($productData);
        }
    }

    private function countStatistics(CommandOutput $commandOutput): void
    {
        $uow = $this->entityManager->getUnitOfWork();
        $uow->computeChangeSets();
        $commandOutput->linesDeleted = \count($uow->getScheduledEntityDeletions());
        $commandOutput->linesInserted = \count($uow->getScheduledEntityInsertions());
        $commandOutput->linesUpdated = \count($uow->getScheduledEntityUpdates());
    }
}